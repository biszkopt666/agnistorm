from django.contrib import admin
from .models import Post, Zdjecia, Osoby, PoleTekstowe, Uslugi, Slajdy, Wideo, Partnerzy

admin.site.register(Post)
admin.site.register(Zdjecia)
admin.site.register(Osoby)
admin.site.register(PoleTekstowe)
admin.site.register(Uslugi)
admin.site.register(Slajdy)
admin.site.register(Wideo)
admin.site.register(Partnerzy)
