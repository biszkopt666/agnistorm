# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from PIL import Image
import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from embed_video.fields import EmbedVideoField
from django.template.defaultfilters import slugify

class Aktualnosci(models.Model):
    tytul = models.CharField(max_length=128)
    tresc = models.TextField(verbose_name='Tresc')
    dodano = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=128, unique=True)

    def save(self, *args, **kwargs):
        self.slug =  slugify(self.tytul)
        super(Aktualnosci, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.tytul

    def __str__(self):
        return self.tytul

    class Meta:
        ordering = ["-dodano"]

class Post(models.Model):
    tytul = models.CharField(max_length=128)
    tresc = models.TextField(verbose_name='Tresc')
    dodano = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.tytul

    def __str__(self):
        return self.tytul

class Slajdy(models.Model):
    foto = models.ImageField(upload_to='slajdy')

    def save(self, *args, **kwargs):
        if self.foto:
            image = Image.open(StringIO.StringIO(self.foto.read()))
            if (image.size[0]/2) > image.size[1]:
                ramka = int((image.size[0]-(2*image.size[1]))/2)
                image = image.crop((ramka,0,image.size[1]+ramka,image.size[1]))
            else:
                ramka = int(((2*image.size[1])-image.size[0])/4)
                image = image.crop((0,ramka,image.size[0],image.size[1]-ramka))
            image.thumbnail((400,200), Image.ANTIALIAS)
            output = StringIO.StringIO()
            image.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.foto= InMemoryUploadedFile(output,'ImageField', "%s" %self.foto.name, 'image/jpeg', output.len, None)
        super(Slajdy, self).save(*args, **kwargs)

class Zdjecia(models.Model):
    foto = models.ImageField(upload_to='zdjecia')
    miniatura = models.ImageField(upload_to='zdjecia')
    kategoria = models.CharField(max_length=128, default='galeria')

    def save(self, *args, **kwargs):
        if self.foto:
            image = Image.open(StringIO.StringIO(self.foto.read()))
            if image.size[0] > image.size[1]:
                image.thumbnail((900,600), Image.ANTIALIAS)
            else:
                image.thumbnail((600,900), Image.ANTIALIAS)
            output = StringIO.StringIO()
            image.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.foto= InMemoryUploadedFile(output,'ImageField', "%s" %self.foto.name, 'image/jpeg', output.len, None)
            male = Image.open(StringIO.StringIO(self.foto.read()))
            if male.size[0] > male.size[1]:
                ramka = int((male.size[0]-male.size[1])/2)
                male = male.crop((ramka,0,male.size[1]+ramka,male.size[1]))
            else:
                ramka = int((male.size[1]-male.size[0])/2)
                male = male.crop((0,ramka,male.size[0],male.size[1]-ramka))
            male.thumbnail((150,150), Image.ANTIALIAS)
            output = StringIO.StringIO()
            male.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.miniatura= InMemoryUploadedFile(output,'ImageField', "mini_%s" %self.foto.name, 'image/jpeg', output.len, None)
        super(Zdjecia, self).save(*args, **kwargs)

class Osoby(models.Model):
    avatar = models.ImageField(upload_to='avatar')
    opis = models.TextField(verbose_name='Opis')
    imie = models.CharField(max_length=32)

    def save(self, *args, **kwargs):
        if self.avatar:
            image = Image.open(StringIO.StringIO(self.avatar.read()))
            if image.size[0] > image.size[1]:
                ramka = int((image.size[0]-image.size[1])/2)
                image = image.crop((ramka,0,image.size[1]+ramka,image.size[1]))
            else:
                ramka = int((image.size[1]-image.size[0])/2)
                image = image.crop((0,ramka,image.size[0],image.size[1]-ramka))
            image.thumbnail((120,120), Image.ANTIALIAS)
            output = StringIO.StringIO()
            image.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.avatar= InMemoryUploadedFile(output,'ImageField', "%s" %self.avatar.name, 'image/jpeg', output.len, None)
        super(Osoby, self).save(*args, **kwargs)

class Uslugi(models.Model):
    nazwa = models.CharField(max_length=64)
    opis = models.TextField()
    slug = models.SlugField(max_length=64)

    def save(self, *args, **kwargs):
        self.slug =  slugify(self.nazwa)
        super(Uslugi, self).save(*args, **kwargs)

class ZdjeciaUslugi(models.Model):
    zdjecie = models.ImageField(upload_to='zdjecia/uslugi')
    miniatura = models.ImageField(upload_to='zdjecia/uslugi')
    kategoria = models.ForeignKey(Uslugi)

    def save(self, *args, **kwargs):
        if self.zdjecie:
            image = Image.open(StringIO.StringIO(self.zdjecie.read()))
            if image.size[0] > image.size[1]:
                image.thumbnail((900,600), Image.ANTIALIAS)
            else:
                image.thumbnail((600,900), Image.ANTIALIAS)
            output = StringIO.StringIO()
            image.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.zdjecie= InMemoryUploadedFile(output,'ImageField', "%s" %self.zdjecie.name, 'image/jpeg', output.len, None)
            male = Image.open(StringIO.StringIO(self.zdjecie.read()))
            if male.size[0] > male.size[1]:
                ramka = int((male.size[0]-male.size[1])/2)
                male = male.crop((ramka,0,male.size[1]+ramka,male.size[1]))
            else:
                ramka = int((male.size[1]-male.size[0])/2)
                male = male.crop((0,ramka,male.size[0],male.size[1]-ramka))
            male.thumbnail((100,100), Image.ANTIALIAS)
            output = StringIO.StringIO()
            male.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.miniatura= InMemoryUploadedFile(output,'ImageField', "mini_%s" %self.zdjecie.name, 'image/jpeg', output.len, None)
        super(ZdjeciaUslugi, self).save(*args, **kwargs)

class PoleTekstowe(models.Model):
    zawartosc = models.TextField(verbose_name="Tresc")

class Wideo(models.Model):
    nazwa = models.CharField(max_length=64, null=True)
    video = EmbedVideoField()
    kategoria = models.CharField(max_length=128, default='galeria')

class Wiadomosci(models.Model):
    autor = models.CharField(max_length=128)
    namiar = models.CharField(max_length=128)
    tresc = models.TextField()
    wyslano = models.DateTimeField(auto_now_add=True)

class Partnerzy(models.Model):
    foto = models.ImageField(upload_to='partnerzy')
    strona = models.URLField(max_length=128)

    def save(self, *args, **kwargs):
        if self.foto:
            image = Image.open(StringIO.StringIO(self.foto.read()))
            image.thumbnail((200,200), Image.ANTIALIAS)
            output = StringIO.StringIO()
            image.save(output, format='JPEG', quality=75)
            output.seek(0)
            self.foto= InMemoryUploadedFile(output,'ImageField', "%s" %self.foto.name, 'image/jpeg', output.len, None)
        super(Partnerzy, self).save(*args, **kwargs)
