from django import forms
from .models import Post, Osoby, PoleTekstowe, Wideo, ZdjeciaUslugi,  Uslugi, Aktualnosci, Partnerzy

class PostForm(forms.ModelForm):
    
    class Meta:
        model = Post
        fields = ('tytul', 'tresc')

class AktualnosciForm(forms.ModelForm):
    
    class Meta:
        model = Aktualnosci
        fields = ('tytul', 'tresc')

class OsobyForm(forms.ModelForm):

    class Meta:
        model = Osoby
        fields = ('imie', 'avatar', 'opis')

class PoleTekstoweForm(forms.ModelForm):

    class Meta:
        model = PoleTekstowe
        fields = ('zawartosc',)

class WideoForm(forms.ModelForm):

    class Meta:
        model = Wideo
        fields = ('nazwa', 'video',)

class ZdjecieUslugiForm(forms.ModelForm):

    class Meta:
        model = ZdjeciaUslugi
        fields = ('zdjecie',)

class UslugiForm(forms.ModelForm):

    class Meta:
        model = Uslugi
        fields = ('nazwa', 'opis',)

class PartnerzyForm(forms.ModelForm):

    class Meta:
        model = Partnerzy
        fields = ('foto', 'strona')
