# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from .models import Aktualnosci, Post, Zdjecia, Osoby, Uslugi, PoleTekstowe, Wideo, ZdjeciaUslugi,  Wiadomosci, Slajdy, Partnerzy
from .forms import AktualnosciForm, PostForm, OsobyForm, PoleTekstoweForm, WideoForm, ZdjecieUslugiForm, UslugiForm, PartnerzyForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

#Glowna
def home(request):
    try:
        PoleTekstowe.objects.get(id=1)
    except ObjectDoesNotExist:
        element = PoleTekstowe(id=1, zawartosc = "Puste....")
        element.save()
    context = {
        "onas": PoleTekstowe.objects.get(id=1),
        "osoby": Osoby.objects.all(),
        }
    return render(request, "index.html", context)

def slajdy(request, szerokosc=None):
    wysokosc = int(szerokosc)/2
    return render(request, "slajdy.html", {"slajdy": Slajdy.objects.all(), "szerokosc": szerokosc, "wysokosc": wysokosc,})

@login_required
def poletekstowe(request, pole_id=None):
    instance = get_object_or_404(PoleTekstowe, pk=pole_id)
    form = PoleTekstoweForm(request.POST or None, instance=instance)
    przekierowanie = request.POST['redirect']
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        if przekierowanie == "index":
            return redirect ("/")
        else:
            return redirect("/%s/"%przekierowanie)
    return render(request, "formularze/poletekstowe.html", {"form": form})

@login_required
def osoby_dodaj(request):
    form = OsobyForm(request.POST, request.FILES) 
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('/')
    return render(request, "formularze/osoby.html", {"form": form})

@login_required
def osoby_edytuj(request, osoby_id=None):
    instance = get_object_or_404(Osoby, pk=osoby_id)
    form = OsobyForm(request.POST or None, instance=instance) 
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('/')
    return render(request, "formularze/osoby.html", {"form": form})

@login_required
def osoby_usun(request, osoby_id=None):
    instance = get_object_or_404(Osoby, pk=osoby_id)
    instance.delete()
    return redirect('/')

#Aktualnosci
def aktualnosci(request):
    newsy = Aktualnosci.objects.all()
    paginator = Paginator(newsy, 10)
    page = request.GET.get('strona')
    try:
        aktualnosci = paginator.page(page)
    except PageNotAnInteger:
        aktualnosci = paginator.page(1)
    except EmptyPage:
        aktualnosci = paginator.page(paginator.num_pages)
    lista_zdjec = []
    lista_filmow = []
    for post in aktualnosci:
        zdjecia = Zdjecia.objects.filter(kategoria=post.slug)
        if zdjecia:
            lista_zdjec.append(post.slug)
        wideo = Wideo.objects.filter(kategoria=post.slug)
        if wideo:
            lista_filmow.append(post.slug)
    context = {
            "aktualnosci": aktualnosci,
            "page_request_var": "strona",
            "strony": range(1,aktualnosci.paginator.num_pages+1),
            "lista_zdjec": lista_zdjec,
            "lista_filmow": lista_filmow,
            }
    return render(request, "aktualnosci.html", context)

def aktualnosci_wpis(request, slug=None):
    instance = get_object_or_404(Aktualnosci, slug=slug)
    return render(request, "wpis.html", {"tresc": instance.tresc,})

@login_required
def aktualnosci_dodaj(request):
    if request.method == "POST":
        form = AktualnosciForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return redirect('/aktualnosci/')
    else:
        raise Http404

@login_required
def aktualnosci_edytuj(request, slug=None):
    if request.method == "POST":
        instance = get_object_or_404(Aktualnosci, slug=slug)
        form = AktualnosciForm(request.POST or None, instance=instance) 
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return redirect('/aktualnosci/')
    else:
        raise Http404

@login_required
def aktualnosci_usun(request, slug=None):
    instance = get_object_or_404(Aktualnosci, slug=slug)
    zdjecia = Zdjecia.objects.filter(kategoria=slug)
    filmy = Wideo.objects.filter(kategoria=slug)
    for zdjecie in zdjecia:
        zdjecie.delete()
    for film in filmy:
        film.delete()
    instance.delete()
    return redirect('/aktualnosci/')

#Oferty
def oferta(request):
    try:
        PoleTekstowe.objects.get(id=2)
    except ObjectDoesNotExist:
        element = PoleTekstowe(id=2, zawartosc = "Puste....")
        element.save()
    context = {
            "uslugi": Uslugi.objects.all(),
            "zdjecia": Zdjecia.objects.all(),
            "filmy": Wideo.objects.all(),
            "opis": PoleTekstowe.objects.get(id=2),
            }
    return render(request, "oferta.html", context)

@login_required
def oferta_dodaj(request):
    if request.method == "POST":
        nazwa = request.POST['nazwa']
        opis = request.POST['opis']
        instance = Uslugi(nazwa=nazwa, opis=opis)
        instance.save()
        return redirect('/oferta/')
    else:
        raise Http404

@login_required
def oferta_edytuj(request, slug=None):
    if request.method == "POST":
        instance = get_object_or_404(Uslugi, slug=slug)
        form = UslugiForm(request.POST or None, instance=instance)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return redirect('/oferta/')
    else:
        raise Http404

@login_required
def oferta_usun(request, slug=None):
    instance = get_object_or_404(Uslugi, slug=slug)
    zdjecia = Zdjecia.objects.filter(kategoria=slug)
    filmy = Wideo.objects.filter(kategoria=slug)
    for zdjecie in zdjecia:
        zdjecie.delete()
    for film in filmy:
        film.delete()
    instance.delete()
    return redirect('/oferta/')

@login_required
def dodaj_zdjecie_do_uslugi(request, slug=None):
    usluga = get_object_or_404(Uslugi, slug=slug)
    form = ZdjecieUslugiForm(request.POST or None, request.FILES or None)
    context = {
            "usluga": usluga,
            "zdjecia": ZdjeciaUslugi.objects.all(),
            "form": form,
            }
    if form.is_valid():
        instance = form.save(commit=False)
        instance.kategoria = usluga
        instance.save()
        return redirect('/oferta/')
    return render(request, "formularze/zdjeciauslugi.html", context)

@login_required
def dodaj_wideo_do_uslugi(request, slug=None):
    usluga = get_object_or_404(Uslugi, slug=slug)
    if request.method == "POST":
        form = WideoForm(request.POST or None) 
        if form.is_valid():
            instance = form.save(commit=False)
            instance.kategoria = usluga
            instance.save()
            return redirect('/oferta/')
    else:
        raise Http404
    
#Galeria
def galeria(request):
    context = {
            "zdjecia": Zdjecia.objects.filter(kategoria="galeria"),
            "filmy": Wideo.objects.filter(kategoria="galeria"),
            }
    return render(request, "galeria.html", context)

def galeria_mini(request, kategoria=None):
    context = {
            "zdjecia": Zdjecia.objects.filter(kategoria=kategoria),
            "kategoria": kategoria,
            }
    return render(request, "minigaleria.html", context)

def wideo_mini(request, kategoria=None):
    context = {
            "kategoria": kategoria,
            "filmy": Wideo.objects.filter(kategoria=kategoria),
            }
    return render(request, "miniwideo.html", context)

@login_required
def zdjecia_usun(request, album=None):
    return render(request, "usun/zdjecia.html", {"zdjecia": Zdjecia.objects.filter(kategoria=album),})

@login_required
def wideo_dodaj(request):
    form = WideoForm(request.POST or None) 
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('/galeria/')
    return render(request, "formularze/wideo.html", {"form": form})

@login_required
def wideo_usun_lista(request, kategoria=None):
    return render(request, "usun/filmy.html", {"filmy": Wideo.objects.filter(kategoria=kategoria)})

#Kontakt
def kontakt(request):
    if request.method == "POST":
        autor = request.POST['autor']
        namiar = request.POST['namiar']
        tresc = request.POST['tresc']
        instance = Wiadomosci(autor=autor, namiar=namiar, tresc=tresc)
        instance.save()
        messages.success(request, 'Wysłano')
        return redirect('/kontakt/')
    context = {
            "wiadomosci": Wiadomosci.objects.all().order_by('-id'),
            "partnerzy": Partnerzy.objects.all()
            }
    return render(request, "kontakt.html", context)

@login_required
def usunwiadomosc(request, wiadomosc_id=None):
    instance = get_object_or_404(Wiadomosci, pk=wiadomosc_id)
    instance.delete()
    messages.success(request, 'Usunięto')
    return redirect('/kontakt/')

@login_required
def partner_dodaj(request):
    form = PartnerzyForm(request.POST, request.FILES) 
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('/kontakt/')

@login_required
def partner_usun(request, partner_id=None):
    instance = get_object_or_404(Partnerzy, pk=partner_id)
    instance.delete()
    return redirect('/kontakt/')

#Upload
@login_required
def dodaj_zdjecia(request, kategoria=None):
    if request.method == "POST":
        przekierowanie = request.POST['redirect']
        for zdjecie in request.FILES.getlist('zdjecia'):
            instance = Zdjecia(foto=zdjecie, kategoria=kategoria)
            instance.save()
        return redirect("/%s/"%przekierowanie)
    else:
        raise Http404("To tak nie działa.")

@login_required
def dodaj_slajdy(request):
    if request.method == "POST":
        for slajd in request.FILES.getlist('slajdy'):
            instance = Slajdy(foto=slajd)
            instance.save()
        return redirect("/")
    else:
        raise Http404("To tak nie działa.")

@login_required
def usun_slajdy(request):
    context = {
            "slajdy": Slajdy.objects.all(),
            }
    return render(request, "usun/slajdy.html", context)

@login_required
def slajd_usun(request, slajd_id=None):
    instance = get_object_or_404(Slajdy, pk=slajd_id)
    instance.delete()
    return redirect('/usun/slajdy/')

@login_required
def dodaj_wideo(request, kategoria=None):
    form = WideoForm(request.POST or None) 
    if request.method == "POST":
        przekierowanie = request.POST['redirect']
        if form.is_valid():
            instance = form.save(commit=False)
            instance.kategoria = kategoria
            instance.save()
        return redirect("/%s/"%przekierowanie)
    else: 
        raise Http404("To tak nie działa.")
#Delete
@login_required
def wideo_usun(request, wideo_id=None):
    instance = get_object_or_404(Wideo, pk=wideo_id)
    przekierowanie = instance.kategoria
    instance.delete()
    return redirect("/wideo/usun/%s/"%przekierowanie)

@login_required
def zdjecie_usun(request, zdjecie_id=None):
    instance = get_object_or_404(Zdjecia, pk=zdjecie_id)
    przekierowanie = instance.kategoria
    instance.delete()
    return redirect("/zdjecia/usun/%s/"%przekierowanie)
 
#inne
def wyloguj(request):
    logout(request)
    return redirect('/')
