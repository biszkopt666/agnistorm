from django.conf.urls import include, url, patterns
from django.contrib import admin
from core import views
from django.conf import settings
from django.contrib.auth.views import login
from django.contrib.auth.decorators import login_required

urlpatterns = [
        #Strona Glowna
    url(r'^$', views.home, name='home'),
    url(r'^slajdy/(?P<szerokosc>\d+)/$', views.slajdy, name='slajdy'),
    url(r'^osoby/dodaj/$', views.osoby_dodaj, name='addartists'),
    url(r'^osoby/edytuj/(?P<osoby_id>\d+)/$', views.osoby_edytuj, name='editartists'),
    url(r'^osoby/usun/(?P<osoby_id>\d+)/$', views.osoby_usun, name='deleteartists'),
    url(r'^usun/slajdy/$', views.usun_slajdy, name='usunslajdy'),
    url(r'^usun/slajd/(?P<slajd_id>\d+)/$', views.slajd_usun, name='usunslajd'),
        #Aktualnosci
    url(r'^aktualnosci/$', views.aktualnosci, name='aktualnosci'),
    url(r'^aktualnosci/tresc/(?P<slug>[\w\-_]+)/$', views.aktualnosci_wpis, name='news'),
    url(r'^aktualnosci/dodaj/$', views.aktualnosci_dodaj, name='addnews'),
    url(r'^aktualnosci/edytuj/(?P<slug>[\w\-_]+)/$', views.aktualnosci_edytuj, name='editnews'),
    url(r'^aktualnosci/usun/(?P<slug>[\w\-_]+)/$', views.aktualnosci_usun, name='deletenews'),
        #Oferta
    url(r'^oferta/$', views.oferta, name='oferta'),
    url(r'^oferta/dodaj/$', views.oferta_dodaj, name='ofertadodaj'),
    url(r'^oferta/edytuj/(?P<slug>[\w\-_]+)/$', views.oferta_edytuj, name='ofertaedytuj'),
    url(r'^oferta/usun/(?P<slug>[\w\-_]+)/$', views.oferta_usun, name='ofertausun'),
    url(r'^oferta/dodajzdjecie/(?P<slug>[\w\-_]+)/$', views.dodaj_zdjecie_do_uslugi, name='dodajzdjeciedouslugi'),
    url(r'^oferta/dodajwideo/(?P<slug>[\w\-_]+)/$', views.dodaj_wideo_do_uslugi, name='dodajwideodouslugi'),
        #Galeria
    url(r'^galeria/$', views.galeria, name='galeria'),
    url(r'^galeria/(?P<kategoria>[\w\-_]+)/$', views.galeria_mini, name='galeria-mini'),
    url(r'^zdjecia/usun/(?P<album>[\w\-_]+)/$', views.zdjecia_usun, name='usunzdjecia'),
    url(r'^zdjecie/usun/(?P<zdjecie_id>\d+)/$', views.zdjecie_usun, name='usunzdjecie'),
    url(r'^wideo/dodaj/$', views.wideo_dodaj, name='addvideo'),
    url(r'^filmy/usun/(?P<kategoria>[\w\-_]+)/$', views.wideo_usun_lista, name='usunfilmy'),
    url(r'^wideo/usun/(?P<wideo_id>\d+)/$', views.wideo_usun, name='usunfilm'),
    url(r'^wideo/(?P<kategoria>[\w\-_]+)/$', views.wideo_mini, name='wideo-mini'),
        #Kontakt
    url(r'^kontakt/$', views.kontakt, name='kontakt'),
    url(r'^kontakt/usun/(?P<wiadomosc_id>\d+)/$', views.usunwiadomosc, name='usunwiadomosc'),
    url(r'^partnerzy/dodaj/$', views.partner_dodaj, name='dodajpartnera'),
    url(r'^partnerzy/usun/(?P<partner_id>\d+)/$', views.partner_usun, name='usunpartnera'),
        #Upload
    url(r'^dodaj/zdjecia/(?P<kategoria>[\w\-_]+)/$', views.dodaj_zdjecia, name='dodajzdjecia'),
    url(r'^dodaj/wideo/(?P<kategoria>[\w\-_]+)/$', views.dodaj_wideo, name='dodajwideo'),
    url(r'^dodaj/slajdy/$', views.dodaj_slajdy, name='dodajslajdy'),
        #Administracja
    url(r'^login/$', login, name='login'),
    url(r'^wyloguj/$', views.wyloguj, name='wyloguj'),
    url(r'^admin/', include(admin.site.urls)),
        #Tymczasowe
    url(r'^edytujpole/(?P<pole_id>\d+)/$', views.poletekstowe, name='edytujpole'),
]
if settings.DEBUG:
    urlpatterns += patterns('', (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))
